beta 0.1.2 (2020-01-01)
-----------------------
### new features
- FAST-FORWARD: you can now fast-forward through a song, useful for getting quickly to the bit you want to record when recording mods & doing many takes (default key: hold #)
- COMPILE TO ONE FILE: you can now "bounce" all of your recorded mods takes to a single mods file! saves to modscompiled.lua (default key: ctrl+E)
- QUICK SAVE + RESTART: pressing restart key (default ctrl+Home) while recording now saves mods and preserves recording mode enabled and current mouse mode, allowing rapid multi-track mods recording and recording mods with mouse at very beginning of a song
- it's now possible to disable particular mouse controls from being recorded for e.g. setting song rate while recording

### bug fixes
- pressing escape now discards recorded mods properly
- skewx is now recorded to three decimal places instead of 1 (hack - in future will be ALL mods that are -1..1 rather than -100..100)
- ctrl and shift keys are now handled correctly if you start a song holding down one then let it go
- "reset all" button now resets mouse mod controls properly
- pressing "reset all" button no longer records when not recording
- resetting two-mod mod controls (e.g. hallway+distant or boost+brake) now resets both mods properly
- stepmania no longer tries to load recorded mods files if they've just been deleted

beta 0.1.1rc (2019-12-18)
-------------------------
- fixed bug with speed mods not being applied correctly (thanks dangan2020)

beta 0.1 (2019-12-17)
---------------------
- initial release

