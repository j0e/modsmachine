#!/usr/bin/env python3

# midireader - from midi2vjoy.py by c0redumb
# https://github.com/c0redumb/midi2vjoy

# rushed and crappy hack-out but it works

import sys ,os#, time, traceback
##from optparse import OptionParser
import pygame.midi
import signal
import tempfile

		
def main():

	# parse arguments
	if len(sys.argv) > 1:
		device = sys.argv[1]
	else:
		device = None

	print()
	
	pygame.midi.init()
	
	devices = []

	# List all the devices and make a choice
	print('Input MIDI devices:')
	for i in range(pygame.midi.get_count()):
		info = pygame.midi.get_device_info(i)
		if info[2]:
			print("", i, "-", info[1].decode())
			devices.append(i)
	#
	
	try:
		d = int(device)
	except:
		d = device or None

	while not d in devices:
		if not d==None:
			print("Device number",d,"out of range. Valid numbers are",", ".join([str(x) for x in devices]))
		try:
			inp=input("\nSelect MIDI device (Q or ctrl+C to quit): ")
		except KeyboardInterrupt:
			print()
			quit()
		try:
			d = int(inp)
		except:
			if inp=="q":
				quit()
			print("Invalid non-integer input \"",inp,"\". Valid numbers are ",", ".join([str(x) for x in devices]),sep="")
			d = None
	#

	# Open the device for testing
	try:
		print('Opening MIDI device:', pygame.midi.get_device_info(d)[1].decode())
		m = pygame.midi.Input(d)
		print('Device opened. Use ctrl-c to quit.')
		#f = open("pipeline", "w")
		f = tempfile.NamedTemporaryFile(dir="", mode="w")
		os.rename(f.name, "pipeline")
		while True:
			while m.poll():
				#print(m.read(1))
				mc = m.read(1)[0][0]
				msgtype, channel = (mc[0]&0xF0)>>4, (mc[0]&0x0f)+1
				msgnames = { 8:"note", 9:"note", 11:"ctrl", 12:"prgm", 15:"syst"}	
				typestr = msgnames[msgtype] if msgtype in msgnames else str(msgtype)
				vel = -1-mc[2] if msgtype==8 else mc[2]
				if msgtype != 15: # ignore system messages for now. TODO ignore clock intelligently
					print("channel %2d    %s %3d    value %d"%(channel, typestr, mc[1], vel))
					f.write( "%d %s %d %d\n"%(channel, typestr, mc[1], vel) )
				f.flush()
	except KeyboardInterrupt:
		print()
	except Exception as e:
		print(e)
	finally:
		try:
			m.close()
		except:
			print()
		f.close()
		os.remove("pipeline")
	
	pygame.midi.quit()

def removepipeline():
	os.remove("pipeline")
	print()

import atexit
atexit.register(removepipeline)

if __name__ == '__main__':
	# handle close button
	signal.signal(signal.SIGINT, removepipeline)
	if sys.platform!="win32":
		#print("Not windows")
		signal.signal(signal.SIGQUIT, removepipeline)
		signal.signal(signal.SIGHUP, removepipeline)
	signal.signal(signal.SIGTERM, removepipeline)
	main()
