-- Archive of MIDI configuration used at UKSRTX.
-- Maps to Korg NanoKONTROL2's default (I think) MIDI mapping.
-- See config_default.lua for more info on what each thing does.

return {

	-- MOD CONTROLS: Analogue (MIDI / Mouse) continuous range mod control settings
	modcontrols = { 

	--CC NUMBER   MOD NAME     MIN   MAX  DEADZONE
		[16]= { "skewx",         1,   -1    },
		[17]= { "drunk",      -250,  250, dead=0  },
		[18]= { "rotationy",   -90,   90, dead=16 },
		[19]= { "mini",        150, -200,   },
		[20]= { "flip",        200, -200, dead=0  },
		[21]= { "invert",     -200,  200, dead=16 },
		[22]= { "x",          -480,  480    },
		[23]= { "rotationz",   -90,   90, dead=8  },
		  --^-- knobs --^--
		
		 [0]= { "boost",       150,    0 ,dead=0,  "brake",   0, 150 },
		 [1]= { "tipsy",      -250,  250    },
		 [2]= { "hallway",     200,    0 ,dead=8,  "distant", 0, 200 },
		 [3]= { "hidden",      100,    0 ,dead=64, "sudden",  0, 100 },
		 [4]= { "reverse1",    100,    0    },
		 [5]= { "reverse2",    100,    0    },
		 [6]= { "reverse3",    100,    0    },
		 [7]= { "reverse4",    100,    0    },
		  --^-- faders --^--

		-- The mouse controls weren't in the UKSRTX mods machine,
		-- but they're here for those who don't have a MIDI controller.
		mouse = {
			{ name="skew / boost+brake",       X={ "skewx",       1,    -1 },          Y={ "boost",   150,    0,         "brake",   0, 150 } },
			{ name="drunk / tipsy",            X={ "drunk",    -250,   250 },          Y={ "tipsy",  -250,  250 } },
			{ name="swivel / hallway+distant", X={ "rotationy",  90,   -90 ,dead=16 }, Y={ "distant", 200,    0, dead=8, "hallway", 0, 200 } },
			{ name="X-offset / reverse",       X={ "x",        -480,   480 },          Y={ "reverse", -25,  100 } },
			{ name="rotation / mini",          X={ "rotationz", -90,    90 },          Y={ "mini",   -200,  150 } },
			{ name="flip / invert",            X={ "flip",      200,  -200 },          Y={ "invert", -200,  200, dead=16 }, }
		},
	},

	-- MESSAGE BUTTON CONTROLS: for digital on/off inputs such as keyboard or MIDI buttons/notes,
	-- trigger messages that cause things to happen (See message handler below)
	-- some idiosyncratic names kept as they were to maintain compatibility when recording mods
	msgbuttons = {

		[58]={ "LUDR",      off="LDUR" },
		[59]={ "Invisible", off="Revisible" },

		[46]=  "RandColSwap",

		-- trick buttons 1-8
		[64]="SpinLeft",
		[65]="SpinRight",
		[66]="Brake", -- nudge
		[67]="Pulse",
		[68]={ "Expand", off="Unexpand" },
		[69]="Sidechain",
		[70]="togglep2reverse",
		[71]={ "Invisible", off="Revisible" },

		-- sine impulse X & Y
		[60]="RndRotXY", -- "bump it"
		[61]="DrunkPulse",
		[62]="TipsyPulse",

		-- rew/ff/stop/play keys
		[43]=  "FlipRev",
		[44]=  "XSpin", -- "revolve", should be Y-spin lol
		[42]=  "WhiteFlash",
		[41]={ "Vibrate", off="Unvibrate" },

		-- Keyboard aliases for above
		q=58, w=46, e=59, r=60, t=61, y=62,

		a=43, s=44, d=42, f=41,

		z=64, x=65, c=66, v=67, b=68, n=69, m=70,

	},

	-- MIDI RESET BUTTON CONTROLS
	midiresetbuttoncontrols = {

		-- knob reset buttons
		[32]=16, [33]=17, [34]=18, [35]=19, [36]=20, [37]=21, [38]=22, [39]=23,
		-- fader reset buttons
		[48]=0,  [49]=1,  [50]=2,  [51]=3,  [52]=4,  [53]=5,  [54]=6,  [55]=7,

		-- The special CC number -1 will reset every mod in the mod table.
		[45]=-1, -- reset all
	},

	-- MESSAGE HANDLER: IMPLEMENTATION DETAILS OF MESSAGES
	msghandler = {

		-- Messages broadcast by keyboard and MIDI buttons are handled here.
		-- When a message is broadcast, the corresponding function in this table is called.

		LUDR=function() MM.mod("flip", 25, 999) MM.mod("invert", -75, 999) end,
		LDUR=function() MM.mod("flip", 0, 999)  MM.mod("invert", 0, 999)   end,

		Invisible = function() MM.mod("stealth", 100, 999) end,
		Revisible = function() MM.mod("stealth",   0, 999) end,

		RandColSwap = function() 
			repeat newcswap = math.random(1,#MM.colswaps) until newcswap ~= MM.curcolswap
			cswap = MM.colswaps[newcswap]
			for i=1,2 do
				MM.mod("flip",   cswap[1], 999, i) 
				MM.mod("invert", cswap[2], 999, i) 
			end
			MM.curcolswap = newcswap
		end,

		-- If you enclose the function with MM.forbothPF( ), the function will be applied to 
		-- both playfields individually!
		Vibrate    = MM.forbothPF( function(self,p) self:vibrate():effectmagnitude(32,32,32) end ),
		Unvibrate  = MM.forbothPF( function(self,p) self:stopeffect() end ),

		RndRotXY = MM.forbothPF(function(self)
			local deg2rad, angle, min,max = (math.pi/180), math.random(0,359), 45,90
			local x = math.sin(angle*deg2rad)*math.random(min,max)
			local y = math.cos(angle*deg2rad)*math.random(min,max)
			self:finishtweening():rotationx(x):rotationy(y):decelerate(0.5):rotationx(0):rotationy(0)
		end),

		SpinLeft = MM.forbothPF(function(self)
			local rot = self:GetRotationZ()
			self:finishtweening():rotationz(rot+360)
				:decelerate(0.5):rotationz(rot)
		end),

		SpinRight = MM.forbothPF(function(self)
			local rot = self:GetRotationZ()
			self:finishtweening():rotationz(rot-360)
				:decelerate(0.5):rotationz(rot)
		end),
		
		Expand   = function() MM.mod("flip", -50, 10) end,
		Unexpand = function() MM.mod("flip",   0, 10) end,
		
		FlipRev=function()
			for i=1,4 do MM.mod("reverse"..i, 100-MM.columnreverse[i], 10) end
		end,
		togglep2reverse=function(self)
			MM.p2reverse = not MM.p2reverse
			for i=1,4 do MM.mod("reverse"..i, MM.columnreverse[i], 10) end
		end,

		XSpin=function(_,p)
			local modul = (-1+math.random(0,1)*2)
			MM.forbothPF( function(self) 
				self:finishtweening():rotationy(0)
				:decelerate(1):rotationy(360*3*modul*(-3+self.pl*2))
				:sleep(0):rotationy(0) 
			end )()
		end,

		ModPulse = function(self,p)
			local modname, amount, returnspeed, cmd2name = p[1], p[2], p[3], p[4]
			if not cmd2name then cmd2name = modname..tostring(amount).."_"..returnspeed end
			if type(amount)=="number" then amount = {amount,amount} elseif type(amount)=="function" then amount = {amount(1),amount(2)} end
			for i=1,2 do MM.mod(modname, amount[i], 999,i) end
			(self:sleep(0.03):GetCommand(cmd2name) and self or self:addcommand(cmd2name,function() MM.console{"Running "..cmd2name, v=2} MM.mod(modname, 0, returnspeed) end)):queuecommand(cmd2name)
		end,

		DrunkPulse = function() MESSAGEMAN:Broadcast("ModPulse", {"drunk", function(pl) return -300*(-1+math.random(0,1)*2) end, 9}) end,
		TipsyPulse = function() MESSAGEMAN:Broadcast("ModPulse", {"tipsy", function(pl) return -300*(-1+math.random(0,1)*2) end, 9}) end,
		Pulse      = function() MESSAGEMAN:Broadcast("ModPulse", {"tiny", -150, 12} ) end,
		Sidechain  = function() MESSAGEMAN:Broadcast("ModPulse", {"mini",  150, 10} ) end,
		Brake      = function() MESSAGEMAN:Broadcast("ModPulse", {"brake", 100,  4} ) end,

		-- Called by resetting CC -1.
		ResetAll=function() MM.p2reverse = false; MM.curcolswap = 1; end,

		-- It is also possible to store objects in the message handler.
		-- Note that because these objects are not the message handler itself,
		-- it is necessary to append MessageCommand to the name of each message.
		Def.Quad { Name="whiteflash",
			OnCommand=cmd(FullScreen; diffuse,0,0,0,0),
			WhiteFlashMessageCommand=function(self,p) local ad=p[1]
				self:finishtweening():blend(ad and Blend.Add or Blend.Normal):diffuse(Color.White)
				:decelerate(ad and 0.3 or 0.5):diffuse(ad and {0,0,0,1} or {1,1,1,0})
			end,
			invertMessageCommand=function(self,p) self:finishtweening():blend(p.on and Blend.Invert or Blend.Normal):diffuse(p.on and {1,1,1,1} or {0,0,0,0}) end,
		},
	},
}