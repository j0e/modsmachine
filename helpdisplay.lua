-- totally rushed and wip keybind display, but better than having to consult the readme constantly
-- will be improved and cleaned up in future (related keys grouped, more readable layout etc)

local keys,strings = ...
local thefont = "_open sans semibold 24px"
local scl = SCREEN_HEIGHT/960

local keylist = Def.ActorFrame { InitCommand=cmd(basezoom,scl) }

local lineheight = 32
local numkeys=0; for _ in pairs(keys) do numkeys=numkeys+1 end -- why isn't there a builtin method for this in lua
local yoffset = lineheight*numkeys/2

-- copypasted from strings.lua
local function keystr(s) return s:upper():gsub("(CTRL%-)(%a+)", "Ctrl+%2"):gsub(" +"," or "); end

-- takes a CamelCaseString and puts spaces where appropriate.
-- handles initialisms correctly (e.g. ToggleAFTVisibility) but not numbers fully
local function spacecamel(s) return s:gsub("([%l%d])(%u)","%1 %2"):gsub("(%u)(%u[%l$])", "%1 %2") end

local i = 0
for action,key in pairs(keys) do
	i=i+1
	local X,Y = SCREEN_CENTER_X/scl/2 , (i*32+SCREEN_CENTER_Y/scl)-yoffset
	keylist[#keylist+1] = Def.BitmapText {
		Text=action=="showkeys" and "guess :)" or (keystr(key)..":"),
		Font=thefont,
		InitCommand=cmd(halign,1; xy,X-16,Y ),
	}
	keylist[#keylist+1] = Def.BitmapText {
		Text=(strings[action] or "j0e hasn't written a description for this key yet!"), -- cuz u know it'll happen eventually
		Font=thefont,
		InitCommand=cmd(halign,0; xy,X+16,Y; ),
	}
end

return Def.ActorFrame {
	Def.Quad {
		InitCommand=cmd(FullScreen; diffuse,{0,0,0,0.75}),
	},
	keylist,
}