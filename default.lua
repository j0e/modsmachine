-- TEH MODS MACHINE! beta v0.1

  ----------------------------------------------------------------------
---- DON'T TOUCH IT, KIDDO (unless you really know what you're doing) ----
---- all user-configurable settings are in config.lua                 ----
  ----------------------------------------------------------------------

-- disclaimer: code's a bit of a mess in some places due to wanting to get it released

--------------------------------------------------------------------------------

-- keep the file handle global in case it wasn't freed correctly last time
-- (such as when DelayedBack is disabled)
if MMfile then
	MMfile:Close()
	MMfile:destroy()
	MMfile=nil
end
MMfile=RageFileUtil.CreateRageFile();

-- namespace "tbl" to avoid namespace clashes
-- (simply love in particular has its own table.tostring that we don't wanna overwrite)
--[[local ]]tbl = LoadActor("utils.lua")

local pwd = ResolveRelativePath("",1); pwd=pwd:sub(1,pwd:find_last("/"))

-- PERSISTENT SETTINGS --
local persistentinipath = ResolveRelativePath("settings.ini",1,true)
if persistentinipath then MMp = IniFile.ReadFile(persistentinipath)["1"]
else
	persistentinipath = pwd.."settings.ini"
	MMp = {
		verbose = 1,
		consolelines = 8,
		autoconnect = false,
		autoplay = false,
		configfile = "config.lua",
		keyboardenabled = true,
		keeprecordingonrestart = false,
	}
end

local function setpersistent(key, value)
	if key then MMp[key]=value end
	IniFile.WriteFile(persistentinipath, {MMp})
end

local function togglepersistent(key)
	setpersistent(key, not MMp[key])
	return MMp[key] -- returns new value for convenience
end

-- check if we're running 5.0 and enable workarounds for features only in 5.1
-- (per-column reverse, mouse buttons, etc)
local sm50 = ProductVersion():sub(1,3)=="5.0"

MM = { -- first, initialise global MM scope with things to be exposed to config.lua
	forbothPF = function(func,param) -- dank function that does something for both players, see config.lua for usage examples
		return function(_,param) 
			for pn=1,2 do if PF[pn] then func(PF[pn],tbl.mergeintonew(param,{player=pn})) end end
		end
	end
}

tbl.overwrite(MM, LoadActor(MMp.configfile)) -- TODO rudimentary default config if none found

tbl.overwrite(MM, {
	keys = tbl.underwrite(MM.keys or {}, { -- defult keymappings
		connect="ctrl-m    space",
		disconnect="escape",
		record="ctrl-r",
		mouse="pgdn",
		mouseback="pgup",
		indicator="ctrl-i",
		resetcurrentmouse="backspace",
		resetallmouse="ctrl-backspace",
		clearmods="ctrl-delete",
		restart="ctrl-home",
		verbose="ctrl-v",
		toggleautoconnect="ctrl-a",
		toggleautoplay="ctrl-p",
		showkeys="shift-/",
		togglekeyboard="ctrl-k",
		fastforward="#",
		compile="ctrl-e",
	}),
	columnreverse = {0,0,0,0},
	p2reverse = false,
	curcolswap=1,
	-- TODO these three can be moved to recmodshandler
	recording = false,
	recorded = {},
	currentrecordedmodlevels = {},
	ctrls=0, alts=0, shifts=0,

})

tbl.underwrite(MM, {
	defaultmultiplier = 50,
	modmousecontrols = MM.modcontrols.mouse or { },
})

-- if isbind(p.key, "connect")
local function isbind(key,action)
	--if not key then return false end
	for ac in action:gmatch("%w+") do
		local keys = MM.keys[ac]
		-- this regex probably needs some tweaking
		for word in string.gmatch(keys, "[%a%-]*[^ ]+") do if key==word then MM.console({v=3, "%s bind handled"}, action) return true end end
	end
	return false
end

local lang=LoadActor("strings.lua")

-- HACK: these are defined in recmodshandler but must be declared here so they're not global
-- HACK HACK: earlier abandoned "refactoring" attemot.
--            none of these are actually used right now except for flashrecindicator
-- lol
local flashrecindicator, isrecording, recmods--[[?]], recordmod

-- TODO: move this function to recmodshandler?
local function recordmod(modname, amount, params)
	--local modstring = "{"..beat..", \""..modname.."\", "..amount.."},"
	local beat = round(GAMESTATE:GetSongBeatNoOffset(), 3)--beat = round(beat,3)
	-- hacky but simple way of only recording most recent mod change per timeslice
	-- avoids excessive duplicates in recorded mods files
	-- and ensures that only most recent change for that mod & time is kept
	if not MM.recorded[beat] then MM.recorded[beat] = {} end
	local currecord=MM.recorded[beat]
	if params then
		if type(params)=="table" then
			if not next(params) then params=nil end -- don't record an empty param table
		else -- passed a non-table as params?
			params={params}
		end
	end
	if modname=="message" then
		if not currecord.message then currecord.message = {} end
		-- FIXME move the deepcopy to handlemsg
		currecord.message[#currecord.message+1] = params and {amount,params} or amount
		--MM.console(tbl.tostring(MM.recorded[beat]))
	else
		amount = round(amount,modname=="skewx" and 3 or 1) -- HACK
		-- keep track of current mod levels to prevent recording redundant repeated mods @ the same value
		if (not MM.currentrecordedmodlevels[modname]) or MM.currentrecordedmodlevels[modname] ~= amount then
			currecord[modname] = amount
		end
		MM.currentrecordedmodlevels[modname] = amount
	end
	flashrecindicator()
	--MM.recorded[#MM.recorded+1] = modstring 
end

for k,f in pairs(PlayerOptions) do PlayerOptions[k:lower()]=f end
local function applymod(modname, amount, multiplier, oneplayer)
	-- TODO if mod doesn't exist, log an error -- probably unsupported sm5.1 mod in sm5.0
	-- todo: resetmod could call handlemod w/ 63.5 rather than applymod w/ 0
	if modname:isanyof{"xmod","cmod","mmod","scrollbpm"} then 
		if amount==0 then
			modname="xmod"; amount=1 -- TODO DETECT SPEEDMOD ON START AND USE THAT
		end
	else amount=amount/100 end
	for pn=oneplayer or 1,oneplayer or 2 do
		local po = GAMESTATE:GetPlayerState("PlayerNumber_P"..pn):GetPlayerOptions('ModsLevel_Song')
		if po[modname] then po[modname](po, amount, multiplier) end
	end
end

local function sm50reverse(x, pn)
	-- this just avoids cluttering up the code below
	local r = (pn==2 and MM.p2reverse) and {
		100-MM.columnreverse[1],
		100-MM.columnreverse[2],
		100-MM.columnreverse[3],
		100-MM.columnreverse[4],
	} or MM.columnreverse
	-- shoutout to XeroOl for this rather clever function
	--function help.reverse(r[1],r[2],r[3],r[4])return'*9999 '..r[1]..' reverse,*9999 '..(-r[1]+r[2]-r[3]+r[4])*0.5 ..' alternate,*9999 '..(-r[1]+r[2]+r[3]-r[4])*0.5 ..' cross,*9999 '..(-r[1]-r[2]+r[3]+r[4])*0.5 ..' split'end
	applymod("reverse",     r[1],                     x, pn)
	applymod("alternate", (-r[1]+r[2]-r[3]+r[4])*0.5, x, pn)
	applymod("cross",     (-r[1]+r[2]+r[3]-r[4])*0.5, x, pn)
	applymod("split",     (-r[1]-r[2]+r[3]+r[4])*0.5, x, pn)
end

-- TODO why are these two separate functions?
local function modnorec(modname, amount, multiplier, oneplayer)
	if not multiplier then multiplier = MM.defaultmultiplier end
	local command = false -- tween commands are handled differently to mods
	for v in ivalues{"rotationy", "rotationz", "skewx", "x"} do if modname==v then command=true end end
	if command then
		for pn=1,2 do if PF[pn] then PF[pn][modname](PF[pn],(modname=="x" and PX[pn] or 0)+amount) end end
	else
		if string.sub(modname,1,7)=="reverse" then -- special case for reverse mod, to handle p2reverse and column reverse
			-- column-specific reverse if number at end of modname, otherwise reverse all
			local rn = tonumber(modname:sub(8,8))
			for i=rn or 1,rn or 4 do MM.columnreverse[i] = amount end
			for pn=1,2 do if PF[pn] then -- TODO if stepmania 5.0 then use xero's special reverse helper function
				if sm50 then -- sm5.0 does not have column-specific reverse so we do it this wacky way instead
					sm50reverse(multiplier, pn)
				else
					applymod(modname, ((pn==2 and MM.p2reverse) and (100-amount) or amount), multiplier, pn)
				end
			end end
		else
			applymod(modname, amount, multiplier, oneplayer)
		end
	end
	MM.console({v=4, "mod %s %d%%"}, modname, amount)
end

local function mod(modname, amount, multiplier, oneplayer)
	modnorec(modname, amount, multiplier, oneplayer)
	if MM.recording then recordmod(modname, amount, {m=multiplier, p=oneplayer}) end
end

-- TODO move modpulse; queuenewcommand; setup MM table here (if above funcs will allow it lol)

local function deadzone(val, dead, max) -- TODO unfuck this function (it works but it's a mess!)
	if not dead then return val end
	local halfmax = max/2
	if math.abs(val-halfmax) < dead/2 then
		--MM.console(val-halfmax.." "..max.." "..dead)
		return halfmax
	end
	local result
	if val >= halfmax then
		result = (val-(halfmax+dead/2))/(halfmax-dead/2) 
	else
		result =  (1-((val)/( halfmax - dead/2 )))*-1 
	end
	return result*halfmax+halfmax
end


local function handlemod(mc, val, max, norec, p)
	local mod = norec and modnorec or mod
	if #mc == 6 then -- {lowermod, lowermin, lowermax, uppermod, uppermin, uppermax, dead=deadzone}
		local dead = (mc.dead or 0)/2
		if val <= (max/2)-dead then
			mod(mc[4], mc[5],nil,p)
			mod(mc[1], scale(val, 0, (max/2)-dead, mc[2], mc[3]),nil,p)
		elseif val >= (max/2)+dead then
			mod(mc[1], mc[3],nil,p)
			mod(mc[4], scale(val, (max/2)+dead, max, mc[5], mc[6]),nil,p)
		else -- within deadzone
			mod(mc[1], mc[3],nil,p); mod(mc[4], mc[5],nil,p)
		end
	else -- {mod, min, max, (optional) deadzone}
		mod(mc[1], scale(mc.dead and deadzone(val,mc.dead,max) or val, 0, max, mc[2], mc[3]),nil,p)
	end
end

MM.handlemod = function(mc,val,oneplayer) handlemod(mc,val,127,true,oneplayer) end

local function handlemsg(msg,on,m)
	-- VALID:
	-- "msg"
	-- { "msg", {param} }
	-- { "msg", off=true/string }
	-- { "msg", {param}, off=true/string }
	--MM.console({type(msg)=="table" and tbl.tostring(msg) or msg, c=Color.Green})
	if type(msg)=="number" then
		if MM.msgbuttons[msg] then 
			handlemsg(MM.msgbuttons[msg],on) end
		return
	end
	local msgname, params
	if on==nil then -- mouse move
		msgname,params = msg[1],msg[2]
	elseif on then
		if type(msg)=="string" then
			msgname=msg
			params = {} -- don't record {on=true} for msgs that are never triggered when on=false
		else msgname=msg[1] end
	elseif type(msg)=="table" and (msg.off or msg[2]==true or msg[3]==true) then
		msgname=type(msg.off)=="string" and msg.off or msg[1]
	end
	if msgname then
		for a in ivalues(msg) do if type(a)=="table" then
			params=tbl.mergeintonew(a, {on=on})
		end end
		-- TODO / HACK this code really needs cleaning up
		if m then params = tbl.mergeintonew(params, {x=m.x, y=m.y, on=on}) end -- TODO change to m.on for mousemsgs that don't check for on/off
		if MM.recording and not msg.norecord then recordmod("message", msgname, params or {on=on})
		else MM.console({lang.msgcons, v=2, c=Color.Yellow}, msgname, params and tbl.tostring(params) or "on="..tostring(on)) end
		MESSAGEMAN:Broadcast(msgname,params or {on=on}) -- do this last in case the messagecommand modifies the param table
	end
end

local colswaps = {
	--{  0,    0},
	{  0,  100},
	{ 25,  -75},
	{ 25,  125},
	{ 75,   75},
	{ 75, -125},
	{100,    0},
	{100, -100},
} MM.colswaps = colswaps

--local function doforbothplayers(func,param) return function(_,param) for pn=1,2 do if PF[pn] then func(PF[pn],tbl.overwrite(param,{player=pn})) end end end end
--function Actor:queuenewcommand(name, func) (self:GetCommand(name) and self or self:addcommand(name,func)):queuecommand(name) return self end

MM.mod = modnorec

local sw,sh = SCREEN_WIDTH*(480/SCREEN_HEIGHT), SCREEN_HEIGHT*(480/SCREEN_HEIGHT)
local scx,scy=sw/2,sh/2

local function mousecoords(mousex, mousey, realcoords, square)
	local mx,my
	if realcoords then mx,my = round(mousex,1),round(mousey,1)
	else
		local mxa = (square and clamp(mousex, SCREEN_WIDTH/2-SCREEN_HEIGHT/2, SCREEN_WIDTH/2+SCREEN_HEIGHT/2) or mousex)
		mx =  round((mxa/SCREEN_WIDTH)*127,2)
		my =  round(((mousey)/SCREEN_HEIGHT)*127,2)
	end
	return mx, my
end

return Def.ActorFrame {
	--FOV=90,
	OnCommand=function(self)
		PF = { SCREENMAN:GetTopScreen():GetChild('PlayerP1'),
		       SCREENMAN:GetTopScreen():GetChild('PlayerP2') }
		PX = { PF[1] and PF[1]:GetX(), PF[2] and PF[2]:GetX() } 
		for i=1,2 do
			if PF[i] then PF[i].pl=i end
		end

		SCREENMAN:GetTopScreen():AddInputCallback( function(event)
			local press_type = ToEnumShortString(event.type)
			if event.button and (press_type=="FirstPress" or press_type=="Release") then
				local thekey = ToEnumShortString(event.DeviceInput.button)
				local isdown = ({FirstPress=true, Release=false})[press_type]
				local mx,my = round(INPUTFILTER:GetMouseX(),1), round(INPUTFILTER:GetMouseY(),1) -- put this here for sm50 mouse button emulation
				if event.DeviceInput.is_mouse then -- mouse button clicked / wheel moved
					local mb = thekey:sub(1,thekey:find(" ")-1)
					if mb=="mousewheel" then
						if isdown then
							local amt = thekey:find("up") and -1 or thekey:find("down") and 1 or 0
							MESSAGEMAN:Broadcast("MouseWheel", {x=mx, y=my, d=amt})
						end
					else
						-- TODO: Move this whole function to keybmousehandler (if it won't increase latency).
						--       If a mouse modcontrol has Left= Right= etc. fields, trigger the messages with scaled mouse coords.
						--       These "raw" messages are not meant to be caught or called by msghandler
						--        (include code to ignore recording them?)
						MESSAGEMAN:Broadcast("MouseButton", {x=mx, y=my, on=isdown, btn=mb})
					end
				else -- keyboard key pressed/released
					local pressingctrl, pressingshift;
					if thekey:find("ctrl")  then pressingctrl  = true; MM.ctrls  = math.max(MM.ctrls  + (isdown and 1 or -1), 0) end
					if thekey:find("shift") then pressingshift = true; MM.shifts = math.max(MM.shifts + (isdown and 1 or -1), 0) end
					local ctrl  = MM.ctrls>0 -- doing it this way accounts for multiple control keys
					local shift = MM.shifts>0 -- doing it this way accounts for multiple shift keys
					if (ctrl  and not pressingctrl)  then thekey="ctrl-"..thekey end
					if (shift and not pressingshift) then thekey="shift-"..thekey end
					--MM.console("%d %s", MM.ctrls, tostring(ctrl))
					MESSAGEMAN:Broadcast(isdown and "KeyPressed" or "KeyReleased", {key=thekey})
					if sm50 then -- stepmania 5.0 doesn't support mouse buttons so use 1 and 2 keys to emulate left/right click
						if thekey=="1" then MESSAGEMAN:Broadcast("MouseButton", {x=mx, y=my, on=isdown, btn="left"}) end
						if thekey=="2" then MESSAGEMAN:Broadcast("MouseButton", {x=mx, y=my, on=isdown, btn="right"}) end
					end
				end
				MM.console({"%s %s", v=3},thekey, isdown and "pressed" or "released")
			end
		end )
		if _G.mmrestart then
			_G.mmrestart = false
			MM.console("Mods machine restarted")
		else MM.console("Mods machine initialised")end
		MM.console({"keys: %s", v=4}, tbl.tostring(MM.keys))
	end, 

	Def.ActorFrame {                                             Name="console",
		UpdateConsoleCommand=function(c,p)
			local lstr,lcol,lvbs = p.s or p[1], p.c or Color.White, p.v or 1
			Trace(lstr); lua.Flush()
			if lvbs > MMp.verbose then return end
			c.lines[c.curline]:diffuse(lcol):settext(lstr):GetWrapperState(1):finishtweening():diffusealpha(1):sleep(3):linear(0.5):diffusealpha(0)
			c.curline=c.curline%c.numlines+1
			for i=1,c.numlines do
				local newi=c.numlines-(i-c.curline)%c.numlines
				c.lines[i]:finishtweening():y((newi-1)*-c.lineheight):diffusealpha(1):decelerate(0.1):y(newi*-c.lineheight):diffusealpha(newi==c.numlines and 0 or 1)
			end
		end,
		InitCommand=function(self)
			self.lines=self:GetChildren("line")["line"]
			self.numlines = #self.lines
			self.curline = 1
			self.textzoom = SCREEN_HEIGHT/960
			self.lineheight = 24*self.textzoom+4
			self.hpad = 8*self.textzoom
			self.w, self.h = SCREEN_WIDTH, self.numlines*self.lineheight
			self:halign(0):valign(1)
			self:xy(0, SCREEN_HEIGHT+1)
			self:AddWrapperState()
			for i,line in ipairs(self.lines) do
				line:zoom(self.textzoom):xy(self.hpad, self.lineheight*(i-1))
			end
			MM.consolef = function(line,...) self:playcommand("UpdateConsole", {line:format(...)}) end
			MM.console = function(t, ...)   self:playcommand("UpdateConsole", type(t)=="table" and tbl.overwrite(t,{t[1]:format(...)}) or {t:format(...)}) end
		end,
		KeyPressedMessageCommand=function(self,p)
			if isbind(p.key, "verbose") then
				setpersistent("verbose", (MMp.verbose%#lang.verbose)+1) -- HACK
				MM.console(lang.verbosechanged, MMp.verbose, lang.verbose[MMp.verbose])
			end
		end,
	}..(function(lines) local lt = {}; for i=1,lines do lt[i+1] = Def.BitmapText { Name="line",
		Font = "_roboto black Bold 24px", Text="", InitCommand=function(self) self:Stroke(Color.Black):shadowcolor(Color.Black):shadowlength(2):halign(0):valign(0):AddWrapperState():diffusealpha(0) end
	} end return lt end)(MMp.consolelines+1),

	Def.BitmapText {
		Font = "_roboto black Bold 24px",
		InitCommand=function(self)
			self:basezoom(480/SCREEN_HEIGHT):xy(SCREEN_CENTER_X, SCREEN_HEIGHT/3):Stroke(Color.Black)
			MM.status = function(line, ...) self:playcommand("Update", {line:format(...)}) end 
		end,
		UpdateCommand=function(self,p)
			local txt,align=p[1],p.align or 0.5
			self:finishtweening():diffusealpha(1):settext(txt):sleep(2):linear(1):diffusealpha(0)
		end,
	},


	Def.ActorFrame {                                         Name="midihandler",
		OnCommand=function(self)
			self.fileopened = false;
			if MMp.autoconnect then self:playcommand("OpenFile")
			else MM.console(lang.connect) end
		end,
		OffCommand=function(self) 
			self:playcommand("CloseFile")
		end,
		OpenFileCommand=function(self)
			if self.fileopened==true then return end -- file already open
			local filepath = ResolveRelativePath("pipeline",1,true)
			if filepath then
				if MMfile:Open(filepath, 1+4) then
					MM.console(lang.connected)
					-- skip to the end to avoid reading all the backed-up midi messages
					while not MMfile:AtEOF() do MMfile:GetLine() end
					MMfile:Seek(MMfile:Tell()-1)
					self:SetUpdateFunction(function(self)
						local theline
						local broadcast=false
						local seekfrom, seekto

						while not MMfile:AtEOF() do
							seekfrom = MMfile:Tell()
							readline = MMfile:GetLine()
							seekto = MMfile:Tell()
							if not (seekfrom==seekto or readline=="") then
								--MM.console(seekfrom.." "..readline.." "..seekto)
								msg = split(" ",readline)
								MESSAGEMAN:Broadcast("Midi", { channel=tonumber(msg[1]), type=msg[2], number=tonumber(msg[3]), value=tonumber(msg[4]) })
							end
						end
						MMfile:GetLine() -- to prevent the last line from being broadcast twice
						MMfile:Seek(seekto-1)
					end)
					self.fileopened=true;
					self:GetChild("indicator"):playcommand("Activate")
				else MM.consolef(lang.openerror, MMfile:GetError()) end
			else MM.console(lang.pfnotfound) end
		end,
		CloseFileCommand=function(self)
			self:SetUpdateFunction(nil); MMfile:Close(); MM.console(lang.disconnected); self.fileopened=false
			self:GetChild("indicator"):playcommand("Deactivate")
		end,
		CancelCommand=function(self)
			SCREENMAN:SystemMessage("This line was run!")
		end,
		KeyPressedMessageCommand=function(self,p) 
			--[[if p.key==MM.keys.connect then]]
			if isbind(p.key, "connect") then
				self:playcommand(self.fileopened and "CloseFile" or "OpenFile")
			end;
			if (p.key==MM.keys.disconnect or p.key==MM.keys.restart) and self.fileopened==true then self:playcommand("CloseFile") end
			if isbind(p.key, "toggleautoconnect") then 
				togglepersistent("autoconnect")
				MM.console(lang.toggleautoconnect, MMp.autoconnect and "enabled" or "disabled")
			end
		end,
		MidiMessageCommand=function(self,p)
			if p.type == "ctrl" then
				local mc = MM.modcontrols[p.number]; if mc then
					handlemod(mc, p.value, 127)
				end
				if MM.midiresetbuttoncontrols[p.number] and p.value==127 then
					-- TODO move this into "handleresetbutton" function
					local rc = MM.midiresetbuttoncontrols[p.number]
					if rc==-1 then -- reset all
						MM.p2reverse = false
						MM.curcolswap = 1
						for k,mc in pairs(MM.modcontrols) do
							if k=="mouse" then -- mc = table of mouse controls in this instance
								-- TODO: reset all MOUSE controls
								for _,mousecontrol in ipairs(mc) do
									if mousecontrol.X then mod(mousecontrol.X[1],0) end
									if mousecontrol.Y then mod(mousecontrol.Y[1],0) end
								end
							else
								mod(mc[1],0)
								if #mc == 6 then mod(mc[4],0) end
							end
						end
						if MM.recording then recordmod("message", "ResetAll") end -- hack
					else
						local mc = MM.modcontrols[rc]
						mod(mc[1],0)
						if #mc == 6 then mod(mc[4],0) end
					end
				end
				local msg = MM.msgbuttons[p.number]
				if msg then handlemsg(msg,p.value>=64) end
			end
		end,
		Def.ActorFrame { Name=                     --[[midihandler]]"indicator",
			InitCommand=cmd(basezoom,SCREEN_HEIGHT/480),
			LoadFont("_roboto black Bold 24px")..{
				Text="MIDI",
				InitCommand=function(self)
					self:halign(1):valign(0):xy(sw-16, 16):playcommand("Deactivate")
					-- TODO if pipeline doesn't exist, make invisible -- if pipeline is created in meantime and opened, make re-visible
				end,
				ActivateCommand=function(self)
					self:diffuse(0,0.5,0,1):Stroke{0, 0.25, 0, 1}
				end,
				DeactivateCommand=function(self)
					self:diffuse(0.5, 0.5, 0.5, 0.5):Stroke(Color.Black)
				end,
				MidiMessageCommand=function(self)
					self:finishtweening():diffuse(0,1,0,1):decelerate(0.3):diffuse(0,0.5,0,1)
				end,
			}
		},
	},

	Def.ActorFrame {                                    Name="keybmousehandler",
		-- Keybind display here?
		Def.ActorMultiVertex { Name="cursor",
			InitCommand=function(self)
				self.mx, self.my = SCREEN_CENTER_X, SCREEN_CENTER_Y
				self:SetDrawState({Mode="DrawMode_LineStrip",First= 1, Num= -1})
				self:SetLineWidth(2):blend(Blend.Add)
				self:SetVertices( { { {SCREEN_CENTER_X, SCREEN_CENTER_Y, 0}, {0,0,0,1} }, { {SCREEN_CENTER_X, SCREEN_CENTER_Y, 0}, Color.White } } )
				self:visible(false) -- mousemode initialises at 0
			end,
		},
		InitCommand=function(self) 
			self:SetFOV(0)
			self.showcursor = true
			if _G.MMpmouse then
				self.mode = _G.MMpmouse
				_G.mmpmouse = nil
			else
				self.mode = 0
			end
			self:GetChild("indicator"):playcommand("Mode",{self.mode})
			self:SetUpdateFunction(function(self) -- check mouse moved loop (clicks handled below)
				local newmousex,newmousey = INPUTFILTER:GetMouseX()+0.2, INPUTFILTER:GetMouseY()+0.2
				if self.mx~=newmousex or self.my~=newmousey then
					local mc, mxa = MM.modmousecontrols[self.mode]
					if mc then
						local mx,my = mousecoords(newmousex, newmousey, mc.realcoords, mc.square)
						--[[if mc.realcoords then mx,my = round(newmousex,1),round(newmousey,1)
						else
							mxa = (mc.square and clamp(newmousex, SCREEN_WIDTH/2-SCREEN_HEIGHT/2, SCREEN_WIDTH/2+SCREEN_HEIGHT/2) or newmousex)
							mx =  round((mxa/SCREEN_WIDTH)*127,2)
							my =  round(((newmousey)/SCREEN_HEIGHT)*127,2)
						end]]
						if mc.msg then
							local p = {x=mx, y=my}
							handlemsg{mc.msg,p,norecord=mc.norecord}
						end
						if mc.X then handlemod(mc.X, mx, mc.realcoords and SCREEN_WIDTH or 127) end
						if mc.Y then handlemod(mc.Y, my, mc.realcoords and SCREEN_HEIGHT or 127) end
					end
					self:GetChild("cursor"):SetVertex(2, {{--[[mxa or ]]newmousex,newmousey,0}})
					self.mx, self.my = newmousex, newmousey
				end
			end)
		end,
		OnCommand=function(self)
		-- TODO: If we start on a mousemode (MM restarted), perform that mousemode's onoff message if it has one
		--       we do it here in case stuff happens in it that depends on the screen
		end,
		OffCommand=function() _G.MMpmouse=nil end,
		KeyPressedMessageCommand=function(self,p)
			if isbind(p.key, "mouse mouseback") then
				self.oldmode = self.mode
				self.mode = (self.mode+(p.key==MM.keys.mouseback and -1 or 1)) % (#MM.modmousecontrols+1)
				if self.oldmode~=0 and MM.modmousecontrols[self.oldmode].onoff then handlemsg({MM.modmousecontrols[self.oldmode].onoff, off=true}, false) end
				if self.mode~=0 and MM.modmousecontrols[self.mode].onoff then handlemsg({MM.modmousecontrols[self.mode].onoff, off=true}, true) end
				self:GetChild("cursor"):visible(self.showcursor and self.mode~=0)
				self:GetChild("indicator"):playcommand("Mode",{self.mode})
			end
			if isbind(p.key, "indicator") then
				self.showcursor = ( not self.showcursor and self.mode ~= 0 )
				self:GetChild("cursor"):visible(self.showcursor)
			end
			if isbind(p.key, "resetcurrentmouse") and self.mode~=0 then
				local mc = MM.modmousecontrols[self.mode]
				-- TODO bipolar mods!
				if mc.X then mod(mc.X[1], 0) end
				if mc.Y then mod(mc.Y[1], 0) end
				if mc.msg then handlemsg{mc.msg, {x=mc.realcoords and SCREEN_CENTER_X or 63.5, y=mc.realcoords and SCREEN_CENTER_Y or 63.5}} end
			end
			if isbind(p.key, "resetallmouse") and self.mode~=0 then
				for mc in ivalues(MM.modmousecontrols) do
					if mc.X then mod(mc.X[1], 0) end
					if mc.Y then mod(mc.Y[1], 0) end
					if mc.msg then handlemsg{mc.msg, {x=mc.realcoords and SCREEN_CENTER_X or 63.5, y=mc.realcoords and SCREEN_CENTER_Y or 63.5}} end
				end
			end
			local msg = MM.msgbuttons[p.key]; if msg and MMp.keyboardenabled then handlemsg(msg,true) end
			-- msg = MM.midiresetbuttoncontrols[p.key]; if msg then handlemsg(msg,true) end
			if isbind(p.key, "restart") then _G.mmrestart = true; _G.MMpmouse=self.mode; SCREENMAN:GetTopScreen():SetPrevScreenName("ScreenGameplay"):begin_backing_out() end
			if p.key=="escape" then _G.MMpmouse=nil end
			if isbind(p.key, "togglekeyboard") then
				local k = togglepersistent("keyboardenabled")
				MM.console("Keyboard %s", k and "enabled" or "disabled")
			end
			if isbind(p.key, "fastforward") then
				GAMESTATE:GetSongOptionsObject("ModsLevel_Song"):MusicRate(3, 999)
			end
		end,
		KeyReleasedMessageCommand=function(self,p)
			local msg = MM.msgbuttons[p.key]
			if msg then handlemsg(msg,false) end
			if isbind(p.key, "fastforward") then
				GAMESTATE:GetSongOptionsObject("ModsLevel_Song"):MusicRate(1, 999)
			end
		end,
		MouseButtonMessageCommand=function(self,p)
			local mc = MM.modmousecontrols[self.mode]
			if mc and mc[p.btn] then
				local mx,my = mousecoords(INPUTFILTER:GetMouseX(), INPUTFILTER:GetMouseY(), mc.realcoords, mc.square)
				handlemsg(mc[p.btn], p.on, {x=mx, y=my})
			end
		end,
		MouseWheelMessageCommand=function(self,p)
			self.mode = (self.mode+p.d) % (#MM.modmousecontrols+1)
			self:GetChild("cursor"):visible(self.showcursor and self.mode~=0)
			self:GetChild("indicator"):playcommand("Mode",{self.mode})				
		end,
		---
		Def.ActorFrame { Name=                    --[[mousehandler]]"indicator",
			InitCommand=function(self)
				self:basezoom(SCREEN_HEIGHT/480):AddWrapperState()
			end,
			ModeCommand=function(self)
				self:GetWrapperState(1):stoptweening():decelerate(0.3):y(-32):sleep(2):decelerate(0.3):y(0)
			end,
			LoadFont("_roboto black Bold 24px")..{
				Text="MOUSE",
				InitCommand=function(self)
					self:halign(1):valign(1):xy(sw-16, sh-8)
					self.barsize=120
				end,
				ModeCommand=function(self,p)
					self:finishtweening():decelerate(0.3)
					if p[1]==0 then 
						self:diffuse(0.5, 0.5, 0.5, 0.5):Stroke(Color.Black)
					else
						self:diffuse(0,0.75,0.75,1):Stroke{0, 0.5, 1, 1}
					end
				end,
			},
			Def.Quad {
				InitCommand=function(self)
					self:halign(1):valign(1):diffuse(0.5, 0.5, 0.5, 0.5)
					self.barw = 128
					self:setsize(self.barw,4)
					self:xy(sw,sh)
				end,
			},
			Def.Quad {
				InitCommand=function(self)
					self:halign(0.5):valign(1):diffuse(1,1,1,1)
					self.w,self.barw = 128/#MM.modmousecontrols, 128
					self:setsize(self.w,4):y(sh)
					self:playcommand("Mode",{0})
				end,
				ModeCommand=function(self,p)
					if self.mode==0 then -- coming from 0
						self:x(p[1]==1 and sw-self.barw or sw)
					end
					self:finishtweening():decelerate(0.2) -- TODO snappier tween here
					if p[1]==0 then
						self:zoomx(0)
						self:x(self.mode==1 and sw-self.barw or sw)
					else
						self:zoomx(1)
						self:x(scale(p[1],1,#MM.modmousecontrols,sw-self.barw+self.w/2,sw-self.w/2))
					end
					self.mode=p[1]
				end,
			},
			LoadFont("_open sans semibold 24px")..{
				InitCommand=function(self)
					self:zoom(2/3):halign(1):valign(0):xy(sw-16, sh+8)
				end,
				ModeCommand=function(self,p)
					local n=#MM.modmousecontrols
						self:settext((p[1]~=0 and (
							MM.modmousecontrols[p[1]].name or
							MM.modmousecontrols[p[1]].msg or
							"(no name)"
						) or "off").." "..p[1].."/"..n)
				end,
			},
		},
	},

	Def.ActorFrame {                                      Name="recmodshandler",
		InitCommand=function(self)
			local playbackmods = {}
			local modfiles,nummods = {}, 0
			local songpath = GAMESTATE:GetCurrentSong():GetSongDir()
			for filename in ivalues(FILEMAN:GetDirListing(songpath)) do
				if filename:startswithanyof{"recmods","mods","rec_"} and filename:sub(-4)==".lua" then
					local nummodsbefore = nummods
					-- for some reason GetDirListing doesn't update very quickly, so if you delete a recmods file
					-- SM sometimes thinks it's still there, tries to load it and errors, fucking everything up
					-- so we wrap the loading in a pcall since there's no way to make LoadActor return nil if file turns out not to exist
					local fileloaded, modfilemods = pcall(function() return LoadActor(songpath..filename) end)
					if fileloaded then for _,mod in pairs(modfilemods or {}) do if mod then nummods=nummods+1; playbackmods[nummods]=mod end end end
					if nummods>nummodsbefore then modfiles[#modfiles+1]=songpath..filename end -- don't keep track of empty mods files
				end
			end
			if nummods>0 or #modfiles>0 then
				self.modfiles=modfiles
				if MMp.autoplay then
					MM.consolef(lang.loadedrecmods, nummods, #modfiles)
					table.sort(playbackmods, function(a,b) return a[1] < b[1] end)
					self.currentbeat = 0
					self.curline = 1
					assert(nummods==#playbackmods)
					self.playbackmods = playbackmods
					self:SetUpdateFunction(function(self) -- playback mods loop
						local beat = GAMESTATE:GetSongBeatNoOffset()
						while self.curline<=nummods and beat>=playbackmods[self.curline][1] do
							local curmod = playbackmods[self.curline]
							if curmod[2]=="message" then MESSAGEMAN:Broadcast(curmod[3], curmod[4]) else modnorec(curmod[2], curmod[3], curmod[4] and curmod[4].m, curmod[4] and curmod[4].p) end
							self.curline=self.curline+1
						end
					end)
				else
					MM.consolef(lang.loadedrecmodsnoautoplay, #modfiles)
				end
			end
			-- TODO define either isrecording() or MM.isrecording() here
			-- TODO move recordmod() here
		end,
		OnCommand=function(self)
			if (_G.mmrestartrec and MMp.keeprecordingonrestart) then
				-- TODO code duplication
				MM.recorded = {}
				MM.recording = true
				MM.consolef(lang.recording)
				self:GetChild("indicator"):playcommand("Activate")
				_G.mmrestartrec = false
			end
		end,
		OffCommand=function(self) 
			if MM.recording then self:playcommand("SaveRecordedMods") end
		end,
		KeyPressedMessageCommand=function(self,p)
			if isbind(p.key, "record") then
				if MM.recording then
					self:playcommand("SaveRecordedMods")
				else
					MM.recorded = {}
					MM.recording = true
					MM.consolef(lang.recording)
					self:GetChild("indicator"):playcommand("Activate")
				end
			end
			if isbind(p.key, "disconnect") then
				MM.recorded = {}
				MM.recording = false
				MM.consolef(lang.recdiscard)
				self:GetChild("indicator"):playcommand("Deactivate")
			end
			if isbind(p.key, "compile") then
				self:playcommand("SaveRecordedMods", {export=self.playbackmods})
			end
			if isbind(p.key, "clearmods") then
				if self.modfiles then
					local curtime = round(GetTimeSinceStart()*1000)
					if self.lastdelpress and curtime-self.lastdelpress < 1000 then
						local f = RageFileUtil.CreateRageFile()
						local ignored = 0
						for fn in ivalues(self.modfiles) do
							if fn:find("recmods%d*%.lua") then -- only delete files named "recmods*.lua"
								if f:Open(fn,10) then f:Close() end
							else ignored=ignored+1 end
						end
						f:destroy()
						f=nil
						MM.consolef(lang.clearedrecmods, #self.modfiles-ignored)
						self.modfiles = nil
					else
						self.lastdelpress=curtime
						MM.console(lang.clearconfirm)
					end
				else MM.console(lang.clearnone)
				end
			end
			if isbind(p.key, "toggleautoplay") then 
				togglepersistent("autoplay")
				MM.console(lang.toggleautoplay, MMp.autoplay and "enabled" or "disabled")
			end
			if isbind(p.key, "restart") then
				if MM.recording and MMp.keeprecordingonrestart then _G.mmrestartrec=true end
				self:playcommand("SaveRecordedMods")
			end
		end,
		SaveRecordedModsCommand=function(self,p)
			-- TODO optimise this
			-- if export parameter is given, then export all loaded mods to single file
			-- otherwise write and save current mods
			-- TODO add current recorded mods to this before exporting?
			if not MM.recording and not p then return end
			local readtable,writetable = p and p.export or {},{"return {"}
			local export = (p and p.export) and true or false
			local i = 0
			-- since recorded mods table keys won't be contiguous
			-- (mods can be recorded at any time) and pairs function
			-- is not guaranteed to be in order, take list of keys
			-- and sort them first, then use this list to iterate
			-- through recorded mods table. This guarantees that
			-- recorded mods are written in chronological order,
			-- making things like cutting and pasting easier.
			if not export then
				for beat in pairs(MM.recorded) do i=i+1; readtable[i]=beat; end
				table.sort(readtable)
				if i==0 then -- if recorded no mods then ignore and exit early
					MM.console(lang.savenomods)
					MM.recording=false
					self:GetChild("indicator"):playcommand("Deactivate")
					return
				end
			end
			i = 2
			if export then
				for line in ivalues(readtable) do
					--MM.console("Adding "..tbl.tostring(line))
					writetable[i] = tbl.tostring(line)..","
					i=i+1
				end
			else
				for beat in ivalues(readtable) do
					local mods = MM.recorded[beat]
					MM.console(tbl.tostring(mods))
					for modname,amount in pairs(mods) do
						if modname=="message" then
							for msgname in ivalues(amount) do
								writetable[i] = table.concat{"\t{",
									beat,
									",\"",
									modname,
									"\",\"",
									(
										type(msgname)=="table"
											and (msgname[1].."\","..tbl.tostring(msgname[2]))
											or msgname.."\""
									),
								"},"}
								i=i+1
							end
						else
							writetable[i] = "\t{"..beat..",\""..modname.."\","..amount.."},"
							i=i+1
						end
					end
				end
			end
			writetable[i] = "}"
			local recfile = RageFileUtil.CreateRageFile()
			-- Save with incrementally numbered file names.
			-- Looks for lowest X such that the file "recmodsX.lua" is not already present in the current song directory
			-- and saves to that file. recmods1.lua, recmods2.lua, recmods3.lua, etc.
			local writefilename
			local ft = {GAMESTATE:GetCurrentSong():GetSongDir(),export and "modscompiled" or "recmods",0,".lua"}
			repeat
				ft[3] = ft[3]+1
				writefilename = table.concat(ft)
			until not FILEMAN:DoesFileExist(writefilename)
			-- TODO catch potential error here
			recfile:Open(writefilename, 10)
			recfile:Write(table.concat(writetable, "\n"))
			recfile:Close()
			recfile:destroy()
			MM.consolef(lang.saved, (i-2), writefilename)
			MM.recording = false
			MM.recorded = {}
			self:GetChild("indicator"):playcommand("Save",{ft[3]})
		end,
		Def.ActorFrame { Name=                              --[[recmodshandler]]"indicator",
			InitCommand=cmd(basezoom,SCREEN_HEIGHT/480),
			LoadFont("_roboto black Bold 24px")..{
				Text="REC",
				InitCommand=function(self)
					self:halign(1):valign(0):xy(sw-16, 48):playcommand("Deactivate")
					flashrecindicator = function() self:playcommand("ModRecorded") end
				end,
				ActivateCommand=function(self)
					self:diffuse(0.5,0,0,1):Stroke{0, 0, 0, 1}
				end,
				DeactivateCommand=function(self)
					self:diffuse(0.5, 0.5, 0.5, 0.5):Stroke(Color.Black)
				end,
				SaveCommand=function(self)
					self:finishtweening():diffuse(0,1,1,1):decelerate(0.5):playcommand("Deactivate")
				end,
				ModRecordedCommand=function(self)
					self:finishtweening():diffuse(1,0,0,1):decelerate(0.3):diffuse(0.5,0,0,1)
				end,
			}
		},
		-- put in a separate file because the code's already busy enough as it is.
		-- soon enough the rest of all of this will be nice and separated and organised,
		-- but for now, this is a start.
		LoadActor("helpdisplay.lua",MM.keys, lang.keys)..{ Name="helpdisplay",
			InitCommand=cmd(visible,false),
			KeyPressedMessageCommand=function(self,p)
				if isbind(p.key, "showkeys") then self:visible(true) end
			end,
			KeyReleasedMessageCommand=function(self,p)
				if isbind(p.key, "showkeys") then self:visible(false) end
			end,
		}
	},

	Def.ActorFrame((function() local t = {} for msg,f in pairs(MM.msghandler) do t[(type(msg)=="string" and msg:sub(-7)~="Command") and (msg.."MessageCommand") or msg]=f end return t end)())
	..{                                                       Name="msghandler",

		--[=[SpinLeftMessageCommand =doforbothplayers(function(self) self:finishtweening():rotationz( 360):decelerate(0.5):rotationz(0) end) ,
		SpinRightMessageCommand=doforbothplayers(function(self) self:finishtweening():rotationz(-360):decelerate(0.5):rotationz(0) end) ,
		NudgeMessageCommand=modpulse("brake", 100, 4),
		PulseMessageCommand=modpulse("tiny", -150, 12),
		ExpandMessageCommand  =function() modnorec("flip", -50, 10) end,
		UnexpandMessageCommand=function() modnorec("flip",   0, 10) end,
		SidechainMessageCommand=modpulse("mini", 150, 10),
		--[[ToggleP2ReverseMessageCommand=function(self)
			MM.p2reverse = not MM.p2reverse
			for i=1,4 do modnorec("reverse"..i, MM.columnreverse[i], 10) end
		end,]]
		InvisibleMessageCommand=function() modnorec("stealth", 100, 999) end,
		RevisibleMessageCommand=function() modnorec("stealth", 0,   999) end,
		--
		BumpItMessageCommand=doforbothplayers(function(self)
			local x,y = xyrot(math.random(0,359), 45, 90)
			self:finishtweening():rotationx(x):rotationy(y):decelerate(0.5):rotationx(0):rotationy(0)
		end),
		DrunkPulseMessageCommand=modpulse("drunk", function() return -300*(-1+math.random(0,1)*2) end, 9),
		TipsyPulseMessageCommand=modpulse("tipsy", function() return -300*(-1+math.random(0,1)*2) end, 9),
		--
		LUDRMessageCommand=function() modnorec("flip", 25, 999) modnorec("invert", -75, 999) end,
		LDURMessageCommand=function() modnorec("flip",  0, 999) modnorec("invert",   0, 999) end,
		RandColSwapMessageCommand=function() 
			repeat newcswap = math.random(1,#colswaps) until newcswap ~= MM.curcolswap
			cswap = colswaps[newcswap]
			for i=1,2 do
				modnorec("flip",   cswap[1], 999, i) 
				modnorec("invert", cswap[2], 999, i) 
			end
			MM.curcolswap = newcswap
		end,
		--
		--[[FlipReverseMessageCommand=function()
			for i=1,4 do modnorec("reverse"..i, 100-MM.columnreverse[i], 10) end
		end,]]
		--[[RevolveMessageCommand=function()
			local modul = (-1+math.random(0,1)*2)
			doforbothplayers( function(self,pn) self:finishtweening():rotationy(0):decelerate(1):rotationy(360*3*modul*(-3+pn*2)):sleep(0):rotationy(0) end )()
		end,]]
		-- whiteflash below
		VibrateMessageCommand  =doforbothplayers( function(self) self:vibrate():effectmagnitude(32,32,32) end ),
		UnvibrateMessageCommand=doforbothplayers( function(self) self:stopeffect() end ),]=]

		ResetAllMessageCommand=function() MM.p2reverse = false; MM.curcolswap = 1; end,
	},

	--[[Def.Quad { Name="whiteflash", -- this can be merged into msghandler above
		OnCommand=cmd(FullScreen; diffuse,0,0,0,0),
		AdditiveWhiteFlashMessageCommand=cmd(finishtweening; blend,Blend.Add; diffuse,1,1,1,1; decelerate,0.3; diffuse,0,0,0,1; ),
		WhiteFlashMessageCommand=cmd(finishtweening; blend,Blend.Normal; diffuse,1,1,1,1; decelerate,0.5; diffuse,1,1,1,0; ),
		invertMessageCommand=function(self,p) local c = p.on and 1 or 0; self:finishtweening():blend(Blend[p.on and "Invert" or "Normal"]):diffuse(c,c,c,c) end,
	},]]
}