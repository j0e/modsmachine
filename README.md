# TEH MODS MACHINE!!!1
beta v0.1 -- dec 2019 -- by j0e -- for stepmania 5.x

---

The Mods Machine is a theme plug-in for Stepmania 5 that adds live real-time mods controls to the game. Now you too can manipulate the threads of fate in the comfort of your own home!

### FEATURES:
* Plugs into any theme, works with any song!*
* Compatible with Stepmania 5.0 and 5.1 (see below)
* Supports keyboard, mouse, and MIDI controller via external utility
* Record and playback mods performances!

(* see bottom of this file)

## Installing

1. Copy the `modsmachine` folder into your Stepmania 5 root directory. It should be alongside the "Songs", "Themes", "NoteSkins", etc. folders.

2. Go into the folder of the theme you want to add the Mods Machine to. In the `BGAnimations` folder, there'll either be a file named `ScreenGameplay overlay.lua`, or a folder named `ScreenGameplay Overlay` with a `default.lua` file inside. Open the respective lua file and scroll down to the bottom. You'll see a line like `return t` -- right above that line, copy and paste this line (without quotes of course): ```t[#t+1] = LoadActor("/modsmachine")``` (and of course, if the bottom line returns something different, for example if it says something like `return af`, change from `t` accordingly.)

3. That's it!

## Running

1. (optional) Run the included MIDI reader utility, `midireader.py`. Requires python 3 and pygame, compiled exe coming soon if you can't or don't want to install python3 and pygame. Pick your device from the menu and make sure you can see the midi messages being printed on-screen when you tweak a control.

2. Run stepmania and play a song. If all went well, you should see a small message saying "Mods machine initialised" at the bottom of the screen. Press Ctrl+M to connect Stepmania to the MIDI reader utility, or mousewheel or PgUp/PgDn to cycle through mouse control modes. Some keyboard buttons do other stuff; press and hold the ? key in-game for a list of all keyboard controls (might be a different button from ? on non-UK/US keyboards).

3. Have fun!

## Recording Mods

Press Ctrl+R to record mods. Press Ctrl+R again while recording to save what you've recorded. If you make a mistake, you can press ESC to exit record mode without saving. Each recorded take is saved to a new file called `recmodsN.lua`, where `N` is a number that increases with each take. These are saved in the folder of the song you recorded the mods for. Separate files for each take allows you to pick and choose what takes to keep, and you can layer mods on top of each other by recording multiple takes! You can press Ctrl+Home to quickly restart a song for quickfire recording.

### Playing back recorded mods

If recorded mods files are detected when you start a song, they are automatically loaded and played back. You can use Ctrl+P to toggle this on and off; restart the song for changes to take effect. The Mods Machine detects and loads all files that begin with `recmods` as well as `mods`; see below.

### Deleting and protecting recorded mods

Press Ctrl+Delete twice to clear all recorded mods for this song. Due to limitations in Stepmania 5's filesystem manager, the Mods Machine cannot completely delete the files; only clear their contents and make them empty. If you like a recorded take and want to prevent it from being accidentally deleted, change `recmods` to `mods` in its file name -- the Mods Machine will only clear recorded mods files that begin with `recmods`.

## Stepmania 5.0

The Mods Machine works with both Stepmania 5.0 and 5.1, with the following caveats and accomodations for 5.0:
* Some mods were added in Stepmania 5.1 that aren't in 5.0. Next version of Mods Machine will optionally warn the user of these. Column-specific reverse *is* supported by means of a rather clever helper function that calculates and translates reverse1..4 into equivalent levels of reverse, alternate, split and cross mods (thanks XeroOl!).
* Mouse movement is supported, but mouse buttons are not. You can use the 1 and 2 keys to emulate left and right mouse buttons.

## Configuring and Everything Else

All key-binds and mods settings (what mods are controlled by keyboard, mouse and MIDI) are defined in `config.lua` -- check that file out for more information on what everything does, and to configure your MIDI device. Everything is explained and documented in that file.

The Mods Machine has been tested successfully with SM5.0 default (legacy), SM5.1 default (lambda), DanceDanceRevolution A, CyberiaStyle Last Approach and Simply Love themes.

There is no doubt that I've missed something and there are bugs I haven't discovered -- if you find one, or a theme that the Mods Machine doesn't work with, be sure to let me know, either on the forum thread you found the Mods Machine at, by opening an issue on the Gitlab page, or reach me on discord at away#8886.

---

j0e 2019
