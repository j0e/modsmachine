function string:startswithanyof(t)
	for s in ivalues(t) do
		if self:sub(1,#s)==s then return true end
	end
	return false
end

function string:startswith(s) return self:startswithanyof{s} end

function string:isanyof(t)
	for s in ivalues(t) do if self==s then return true end end
	return false
end

local function table2string(r)
	if r==nil then return r end
	local t =  type(r)=="function" and {r(1),r(2)} or r -- what was I doing here?
	local ret = {}
	local tostring2 = function(v) return (
		type(v):isanyof{"table","function"} and table2string(v) or
		(type(v)=="number" or type(v)=="boolean") and tostring(v) or
		"\""..tostring(v).."\""
	) end
	local done = {}
	for k,v in ipairs(t) do ret[#ret+1] = tostring2(v); done[k]=true end
	for k,v in pairs(t) do if not done[k] then
		ret[#ret+1] = (type(k)=="string" and k or "["..k.."]").."="..tostring2(v)
	end end
	return "{"..table.concat(ret,",").."}"
end

return {
	-- table merging functions -- TODO make these recursive for tables within tables
	overwrite = function(t,y) if t==nil then t={} end; for k,v in pairs(y) do t[k]=v end return t end,
	underwrite = function(t,y) for k,v in pairs(y) do  if not t[k] then t[k]=v end end return t end,

	-- shallow copy, won't handle table keys/vals properly
	mergeintonew = function(t,y)
		local r = {}
		if not t or not y then return t or y or r end
		for k,v in pairs(t) do r[k]=v end
		for k,v in pairs(y) do r[k]=v end
		return r
	end,

	extract = function(from, which)
		if not which then return {}
		elseif type(which)~="table" then which={which}
		end
		local ret = {}
		for k in ivalues(which) do
			ret[k] = from[k]
			from[k] = nil
		end
		return ret
	end,

	tostring=table2string,
}