-- All mods machine configuration is handled here.
-- It's recommended you make a copy or backup of this before editing anything.
-- You can change what config file the Mods Machine uses in settings.ini (written after first run).

return {

	-- KEY BINDINGS
	-- If any of these are missing from config.lua, the default will be used.
	-- These ones here are a copy of the default key bindings.
	-- Descriptions for these can be found by holding the ? key in-game (might be different for non-UK/US keyboards)
	-- or check out strings.lua
	keys = {
		connect           = "ctrl-m space enter", -- multiple keybinds separated by spaces are possible
		disconnect        = "escape",
		record            = "ctrl-r",
		mouse             = "pgdn",
		mouseback         = "pgup",
		indicator         = "ctrl-i",
		resetcurrentmouse = "backspace",
		resetallmouse     = "ctrl-backspace",
		clearmods         = "ctrl-delete",
		restart           = "ctrl-home",
		verbose           = "ctrl-v",
		toggleautoconnect = "ctrl-a",
		toggleautoplay    = "ctrl-p",
		showkeys          = "shift-/", -- the ? key on uk/us keyboards
		togglekeyboard    = "ctrl-k",
	},

	-- MOD CONTROLS: Analogue (MIDI / Mouse) continuous range mod control settings
	modcontrols = { 
		-- Each key [bracketed number] in this list corresponds to a MIDI CC number.
		-- To configure your MIDI device, you can check what control on it maps to what CC number
		-- in the MIDI utility. Change bracketed numbers accordingly.
		--
		-- Each control number has a corresponding mod that it affects, and two values that denote
		-- the lowest and highest percentages the mod will be at when the corresponding control is
		-- at 0 and 127. You'll notice that some entries have another set of mod values -- in this
		-- instance, the first set of mods map to the range 0-63 on the control and the second set
		-- map to 64-127. This is how you get two mods such as Hallway/Distant on a single slider.
		-- The optional "dead" parameter is the size of the deadzone in the middle of the controller,
		-- which defaults to zero. Order matters for everything except the deadzone parameter.

	--CC NUMBER   MOD NAME     MIN   MAX  DEADZONE
		[16]= { "skewx",         1,   -1    },
		[17]= { "drunk",      -250,  250, dead=0  },
		[18]= { "rotationy",   -90,   90, dead=16 },
		[19]= { "mini",        150, -200,   },
		[20]= { "flip",        200, -200, dead=0  },
		[21]= { "invert",     -200,  200, dead=16 },
		[22]= { "x",          -480,  480    },
		[23]= { "rotationz",   -90,   90, dead=8  },
		  --^-- knobs --^--
		
		 [0]= { "boost",       150,    0 ,dead=0,  "brake",   0, 150 },
		 [1]= { "tipsy",      -250,  250    },
		 [2]= { "hallway",     200,    0 ,dead=8,  "distant", 0, 200 },
		 [3]= { "hidden",      100,    0 ,dead=64, "sudden",  0, 100 },
		 [4]= { "reverse1",    100,    0    },
		 [5]= { "reverse2",    100,    0    },
		 [6]= { "reverse3",    100,    0    },
		 [7]= { "reverse4",    100,    0    },
		  --^-- faders --^--

		-- Below is a list of mouse control presets which can be cycled through using the mouse wheel or pgup/pgdn keys.
		-- The X and Y axis controls follow the same format as above.
		-- Since MIDI controls go from 0 to 127, mouse coordinates are also scaled to 0..127
		-- to maintain compatibility between different theme resolutions and aspect ratios
		-- for mods recording and playback. Floating-point is used so no precision is lost.
		-- One thing to note is the Y-axis is the opposite way round to most vertical faders
		-- found on MIDI controllers -- the top is minimum and the bottom is maximum. Map mods accordingly.

		-- Some mouse controls broadcast a message on mouse movement and button press.
		-- The X and Y coordinates of the mouse are broadcast in this message for more advanced mouse-based controls.
		-- See below for more information on messages.
		-- Additionally, it is possible to broadcast a message when this mouse mode is selected or moved away from,
		-- using the "onoff" parameter.
		mouse = {
			{ name="Drunk / Tipsy",               X={ "drunk",-250,250 }, Y={ "tipsy",-250,250 } },
			{ name="Drunk / Tipsy (alternative)", msg="dtdiffplayers" },
			{ name="Spacing / Speed",             X={ "flip", 50, -50 }, Y={ "cmod", 1, 700 } },
			{ name="Angle",                       X={ "rotationy",90,-90, dead=16, },    Y={ "distant", 200, 0, dead=8, "hallway", 0, 200 } },
			{ name="Click to drag playfields!",   msg="PlayfieldDragMoveMouse", left={"ToggleDrag", {1}, off=true}, right={"ToggleDrag", {2}, true}, onoff="ExitDrag" },
			{ name="Spinny",                      X={ "confusionoffset", -150, 150 }, Y={ "roll", -400, 400 } },
			{ name="Tilt 'n' Size",               X={ "rotationz", -90, 90}, Y={"mini", -200, 150} },
			{ name="Flinvert (click+drag)",       msg="flinvert", left={"ToggleDrag", {1}, off=true}, right={"ToggleDrag", {2}, true}, onoff="ExitDrag" },
			{ name="Tilt-o-rama",                 msg="TiltORama", square=true },
			{ name="Spinny Spotlight",            msg="dizdiffplayers",    Y={ "sudden",200,0,  "hidden",0,200 } },
			{ name="Rotate preserving direction", msg="rotateCO" },
			-- TODO: alias X and Y to MIDI controls like with message buttons.
		},
	},

	-- MESSAGE BUTTONS: for digital on/off inputs such as keyboard or MIDI buttons/notes,
	-- trigger messages that cause things to happen (See message handler below)
	msgbuttons = {

		-- Each letter/word as seen below corresponds to a key on the keyboard.
		-- The corresponding message is broadcast when that key is pressed.
		m = "ToggleP2Reverse",
		d = "WhiteFlash",

		-- For more options you can use a table instead of a string.
		-- Passing in off=true means the message will be broadcast when the key is released,
		-- as well as pressed. The press/release boolean will be passed to the command,
		-- so you can do different things on a press or release with one message.
		q = { "LUDR",      off=true },
		i = { "invert",    off=true },

		-- Or, if you prefer, you can set the "off" parameter to a string and broadcast
		-- a different message on key release.
		f = { "Vibrate", off="StopEffect" },

		-- You can also alias keys to MIDI CCs.
		-- This line makes the "w" key broadcast a "RandColSwap" message.
		w = 46,

		-- As above, using a bracketed number instead of a letter binds the message to a MIDI
		-- control. Most momentary-push buttons on MIDI controllers send a CC with a level of
		-- 127 when pressed, and 0 when released. When receiving MIDI message button signals, 
		-- a CC level 64 and above is considered a press, and 63 and below a release.
		[58]={ "LUDR",      off=true },
		[59]={ "Invisible", off=true },

		[46]=  "RandColSwap",

		[60]=  "BumpIt",

		-- You can include parameters with messages, much like you can with function calls.
		[64]={ "Spin", {amt= 360} },
		[65]={ "Spin", {amt=-360} },

		-- ModPulse is an awesome macro that sets the mod named in the first argument
		-- to the level of the second argument, returning back to 0 at the speed of the third argument.
		-- Great for any sort of impact, bumpin' or str0ng beat effect. DOOF DOOF!
		[66]={ "ModPulse", {"brake", 100,  4} }, -- nudge
		[67]={ "ModPulse", {"tiny", -150, 12} }, -- pulse
		[68]={ "Expand", off=true },
		[69]={ "ModPulse", {"mini",  150, 10} }, -- sidechain
		[70]=  "ToggleP2Reverse",
		[71]={ "Invisible", off=true },

		-- sine impulse X & Y, random +/-300% for each player
		-- You can even use a function for the value in modpulse!
		-- The function can use the "pl" argument to do different things for each player (1 or 2),
		-- but it is not used here.
		[61]={ "ModPulse", {"drunk", function(pl) return -300*(-1+math.random(0,1)*2) end, 9} },
		[62]={ "ModPulse", {"tipsy", function(pl) return -300*(-1+math.random(0,1)*2) end, 9} },

		[43]=  "FlipReverse",
		[44]=  "Revolve",
		[42]=  "WhiteFlash",
		[41]={ "Vibrate", off="StopEffect" },

		g = {"co", {1}, true},
		h = {"co", {2}, true},

		-- More keyboard aliases for all the MIDI controls above
		z=64, x=65, c=66, v=67, b=68, n=69, 

		a=43, s=44, e=59, r=60, t=61, y=62,

	},

	-- MIDI RESET BUTTON CONTROLS
	midiresetbuttoncontrols = {

		-- Pretty straightforward, each bracketed number is the CC of the reset button, and
		-- after the equals sign is the CC that button will reset. Pressing the reset button
		-- will reset the mod that CC is linked to in the mod table above.
		-- Read [A]=B as "Control A will reset the mod controlled by B".

		-- knob reset buttons
		[32]=16, [33]=17, [34]=18, [35]=19, [36]=20, [37]=21, [38]=22, [39]=23,
		-- fader reset buttons
		[48]=0,  [49]=1,  [50]=2,  [51]=3,  [52]=4,  [53]=5,  [54]=6,  [55]=7,

		-- The special CC number -1 will reset every mod in the mod table.
		[45]=-1, -- reset all
	},

	-- MESSAGE HANDLER: IMPLEMENTATION DETAILS OF MESSAGES
	msghandler = {

		-- Messages broadcast by keyboard and MIDI buttons are handled here.
		-- When a message is broadcast, the corresponding function in this table is called.

		-- Note the use of p.on to distinguish between button press and release.
		LUDR = function(_,p)
			MM.mod("flip",   p.on and  25 or 0, 999)
			MM.mod("invert", p.on and -75 or 0, 999)
		end,

		Invisible = function(_,p) MM.mod("stealth", p.on and 100 or 0, 999) end,

		RandColSwap = function() 
			repeat newcswap = math.random(1,#MM.colswaps) until newcswap ~= MM.curcolswap
			cswap = MM.colswaps[newcswap]
			for i=1,2 do
				MM.mod("flip",   cswap[1], 999, i) 
				MM.mod("invert", cswap[2], 999, i) 
			end
			MM.curcolswap = newcswap
		end,

		RandColSwapPerPlayer=function() 
			for i=1,2 do
				newcswap = math.random(1,#MM.colswaps)
				cswap = MM.colswaps[newcswap]
				MM.mod("flip",   cswap[1], 999, i) 
				MM.mod("invert", cswap[2], 999, i) 
			end
		end,

		-- If you enclose the function with MM.forbothPF( ), the function will be applied to 
		-- both playfields individually!
		Vibrate    = MM.forbothPF( function(self,p) self:vibrate():effectmagnitude(32,32,32) end ),
		StopEffect = MM.forbothPF( function(self,p) self:stopeffect() end ),

		BumpIt = MM.forbothPF(function(self)
			local deg2rad, angle, min,max = (math.pi/180), math.random(0,359), 45,90
			local x = math.sin(angle*deg2rad)*math.random(min,max)
			local y = math.cos(angle*deg2rad)*math.random(min,max)
			self:finishtweening():rotationx(x):rotationy(y):decelerate(0.5):rotationx(0):rotationy(0)
		end),
			
		Spin = MM.forbothPF(function(self,p)
			local rot = self:GetRotationZ()
			self:finishtweening():rotationz(rot+p.amt)
				:decelerate(0.5):rotationz(rot)
		end),
		
		Expand = function(_,p) MM.mod("flip", p.on and -50 or 0, 10) end,
		
		FlipReverse=function()
			for i=1,4 do MM.mod("reverse"..i, 100-MM.columnreverse[i], 10) end
		end,
		ToggleP2Reverse=function(self)
			MM.p2reverse = not MM.p2reverse
			for i=1,4 do MM.mod("reverse"..i, MM.columnreverse[i], 10) end
		end,

		Revolve=function(_,p)
			local modul = (-1+math.random(0,1)*2)
			MM.forbothPF( function(self) 
				self:finishtweening():rotationy(0)
				:decelerate(1):rotationy(360*3*modul*(-3+self.pl*2))
				:sleep(0):rotationy(0) 
			end )()
		end,

		-- Usage: {"ModPulse", {"modname", amount, returnspeed} }
		-- Pulses the mod modname by amount, returning to 0 at a speed of returnspeed
		ModPulse = function(self,p)
			local modname, amount, returnspeed, cmd2name = p[1], p[2], p[3], p[4]
			if not cmd2name then cmd2name = modname..tostring(amount).."_"..returnspeed end
			if type(amount)=="number" then amount = {amount,amount} elseif type(amount)=="function" then amount = {amount(1),amount(2)} end
			for i=1,2 do MM.mod(modname, amount[i], 999,i) end
			(self:sleep(0.03):GetCommand(cmd2name) and self or self:addcommand(cmd2name,function() MM.console{"Running "..cmd2name, v=2} MM.mod(modname, 0, returnspeed) end)):queuecommand(cmd2name)
		end,

		-- Playfield dragging
		getClosestPlayfieldCommand=function(self,p)
			local px, py = p.x * (SCREEN_WIDTH/127), p.y * (SCREEN_HEIGHT/127)
			self.orig = { { x=PF[1]:GetX(), y=PF[1]:GetY() }, { x=PF[2]:GetX(), y=PF[2]:GetY() } }
			local p1dist = math.sqrt( (px-self.orig[1].x)^2 + (py-self.orig[1].y)^2 )
			local p2dist = math.sqrt( (px-self.orig[2].x)^2 + (py-self.orig[2].y)^2 )
			self.closest = p1dist<p2dist and 1 or 2
			self.clickx, self.clicky = px, py; 
		end,
		ToggleDrag=function(self,p) self["drag"..p[1]] = p.on; if p.on then self:playcommand("getClosestPlayfield",p) end end,
		PlayfieldDragMoveMouse=function(self,p)
			local px, py = p.x * (SCREEN_WIDTH/127), p.y * (SCREEN_HEIGHT/127) -- TODO mx,my = MM.getrealcoords(p)
			local j = self.closest
			if self.drag1 or self.drag2 then PF[j]:xy(self.orig[j].x+(px-self.clickx), self.orig[j].y+(py-self.clicky)) end
			if self.drag2 then
				PF[3-j]:xy(SCREEN_WIDTH-(self.orig[j].x+(px-self.clickx)), SCREEN_HEIGHT-(self.orig[j].y+(py-self.clicky)))
			end
		end,
		ExitDrag=function(self,p) if not p.on then self.drag1=false self.drag2=false end end,

		flinvert = function(self,p)
			if self.drag1 then
				MM.handlemod({"flip", -100, 100}, p.x)
			end
			if self.drag2 then
				MM.handlemod({"invert", -100, 100}, p.x)
			end
		end,

		shakewholescreen=function(_,p)
			if p.on then
				SCREENMAN:GetTopScreen():vibrate():effectmagnitude(32,32,32)
			else
				SCREENMAN:GetTopScreen():stopeffect()
			end
		end,

		TiltORama=function(self,p)
			if SCREENMAN:GetTopScreen():GetNumWrapperStates()>0 then
				SCREENMAN:GetTopScreen():GetWrapperState(1):rotationx(scale(p.y,0,127,-30,30)):rotationy(scale(p.x,0,127,30,-30))
			else
				SCREENMAN:GetTopScreen():SetFOV(90):xy(-SCREEN_WIDTH/2,-SCREEN_HEIGHT/2):AddWrapperState():Center()
			end
		end,

		co=function(_,p) MM.mod("confusion",p.on and -100+math.random(0,1)*200 or 0,50,p[1]) end,

		rotateCO=function(_,p) 
			local corange = 630
			MM.handlemod( {"rotationz",       -360, 360}, p.x )
			MM.handlemod( {"confusionoffset", corange, -corange}, p.x )
		end,

		dtdiffplayers=function(self,p) 
			MM.handlemod({"drunk", -250, 250}, p.x, 1)
			MM.handlemod({"drunk", 250, -250}, p.x, 2)
			MM.handlemod({"tipsy", -250, 250}, p.y)
		end,
		dizdiffplayers=function(self,p) 
			local mxamt,myamt = { scale(p.x,0,127,-90,90), scale(p.x,127,0,-90,90) }, { scale(p.y,0,127,-90,90), scale(p.y,127,0,-90,90) }
			for i=1,2 do MM.mod("dizzy",mxamt[self.drag1 and 1 or i],999,i) end
		end,

		-- Called by resetting CC -1.
		ResetAll=function() MM.p2reverse = false; MM.curcolswap = 1; end,

		-- It is also possible to store objects in the message handler.
		-- Note that because these objects are not the message handler itself,
		-- it is necessary to append MessageCommand to the name of each message.
		Def.Quad { Name="whiteflash",
			OnCommand=cmd(FullScreen; diffuse,0,0,0,0),
			WhiteFlashMessageCommand=function(self,p) local ad=p[1]
				self:finishtweening():blend(ad and Blend.Add or Blend.Normal):diffuse(Color.White)
				:decelerate(ad and 0.3 or 0.5):diffuse(ad and {0,0,0,1} or {1,1,1,0})
			end,
			invertMessageCommand=function(self,p) self:finishtweening():blend(p.on and Blend.Invert or Blend.Normal):diffuse(p.on and {1,1,1,1} or {0,0,0,0}) end,
		},
	},
}